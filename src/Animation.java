import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Animation extends JFrame {
	private DrawingPanel drawingPanel;
	private ControlPanel controlPanel;
	
	public Animation(String title) {
		super(title);
		
		setLayout(new FlowLayout());
		
		drawingPanel = new DrawingPanel(400,400);
		
		add(drawingPanel);
	}
	
	public class DrawingPanel extends JPanel {
		
		public DrawingPanel(int width,
				            int height) {
			setPreferredSize(new Dimension(width,height));
		}
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			g.setColor(Color.white);
			
			g.fillRect(0, 0, 400, 400);
		}
		
	}
	
	public class ControlPanel extends JPanel {
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Animation animation = new Animation("Animation");
		
		animation.setSize(500,500);
		animation.setLocationRelativeTo(null);
		animation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		animation.setVisible(true);
	}

}
